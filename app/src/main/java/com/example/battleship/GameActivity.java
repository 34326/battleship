package com.example.battleship;

import static android.os.Build.ID;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;


public class GameActivity extends AppCompatActivity {

    public Button A11,A21,A31,A41,A51,A61,A71,A81,A91,A101,B11,B21,B31,B41,B51,B61,B71,B81,B91,B101,C11,C21,C31,C41,C51,C61,C71,C81,C91,C101,D11,D21,D31,D41,D51,D61,D71,D81,D91,D101, E11,E21,E31,E41,E51,E61,E71,E81,E91,E101,F11,F21,F31,F41,F51,F61,F71,F81,F91,F101,G11,G21,G31,G41,G51,G61,G71,G81,G91,G101,H11,H21,H31,H41,H51,H61,H71,H81,H91,H101,I11,I21,I31,I41,I51,I61,I71,I81,I91,I101,J11,J21,J31,J41,J51,J61,J71,J81,J91,J101;
    public Button A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10, E1,E2,E3,E4,E5,E6,E7,E8,E9,E10,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,G1,G2,G3,G4,G5,G6,G7,G8,G9,G10,H1,H2,H3,H4,H5,H6,H7,H8,H9,H10,I1,I2,I3,I4,I5,I6,I7,I8,I9,I10,J1,J2,J3,J4,J5,J6,J7,J8,J9,J10;
    TextView textView;
    private GameBoard ground = GameActivity2.getBoard();
    public GameBoard groundAI;
    private double progress_Player;
    private double progress_AI;
    private int AI_difficulty;
    private int HP_player;
    private int HP_AI;
    public static String winner;
    //public ProgressBar progressBar_Player;
    //public ProgressBar progressBar_AI;
    public String difficulty;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        System.out.println("Tablica gracza w GA:");
        for(int i=1;i<ground.ground_tab.length;i++){
            System.out.print(ground.ground_tab[i]);
            if(i%10==0)System.out.println("");
        }
        int min=1;
        int max=4;
        int rand_position_AI = (int)Math.floor(Math.random()*(max-min+1)+min);
        groundAI = new GameBoard(rand_position_AI);
        System.out.println("Tablica AI:");
        for(int i=1;i<groundAI.ground_tab.length;i++){
            System.out.print(groundAI.ground_tab[i]);
            if(i%10==0)System.out.println("");
        }
        AI_difficulty=65;
        HP_player=16;
        HP_AI=10;
        //progress_AI=1.0;
        //progress_Player=1.0;
        A1 = findViewById(R.id.A1);
        A2 = findViewById(R.id.A2);
        A3 = findViewById(R.id.A3);
        A4 = findViewById(R.id.A4);
        A5 = findViewById(R.id.A5);
        A6 = findViewById(R.id.A6);
        A7 = findViewById(R.id.A7);
        A8 = findViewById(R.id.A8);
        A9 = findViewById(R.id.A9);
        A10 = findViewById(R.id.A10);

        B1 = findViewById(R.id.B1);
        B2 = findViewById(R.id.B2);
        B3 = findViewById(R.id.B3);
        B4 = findViewById(R.id.B4);
        B5 = findViewById(R.id.B5);
        B6 = findViewById(R.id.B6);
        B7 = findViewById(R.id.B7);
        B8 = findViewById(R.id.B8);
        B9 = findViewById(R.id.B9);
        B10 = findViewById(R.id.B10);

        C1 = findViewById(R.id.C1);
        C2 = findViewById(R.id.C2);
        C3 = findViewById(R.id.C3);
        C4 = findViewById(R.id.C4);
        C5 = findViewById(R.id.C5);
        C6 = findViewById(R.id.C6);
        C7 = findViewById(R.id.C7);
        C8 = findViewById(R.id.C8);
        C9 = findViewById(R.id.C9);
        C10 = findViewById(R.id.C10);

        D1 = findViewById(R.id.D1);
        D2 = findViewById(R.id.D2);
        D3 = findViewById(R.id.D3);
        D4 = findViewById(R.id.D4);
        D5 = findViewById(R.id.D5);
        D6 = findViewById(R.id.D6);
        D7 = findViewById(R.id.D7);
        D8 = findViewById(R.id.D8);
        D9 = findViewById(R.id.D9);
        D10 = findViewById(R.id.D10);

        E1 = findViewById(R.id.E1);
        E2 = findViewById(R.id.E2);
        E3 = findViewById(R.id.E3);
        E4 = findViewById(R.id.E4);
        E5 = findViewById(R.id.E5);
        E6 = findViewById(R.id.E6);
        E7 = findViewById(R.id.E7);
        E8 = findViewById(R.id.E8);
        E9 = findViewById(R.id.E9);
        E10 = findViewById(R.id.E10);

        F1 = findViewById(R.id.F1);
        F2 = findViewById(R.id.F2);
        F3 = findViewById(R.id.F3);
        F4 = findViewById(R.id.F4);
        F5 = findViewById(R.id.F5);
        F6 = findViewById(R.id.F6);
        F7 = findViewById(R.id.F7);
        F8 = findViewById(R.id.F8);
        F9 = findViewById(R.id.F9);
        F10 = findViewById(R.id.F10);

        G1 = findViewById(R.id.G1);
        G2 = findViewById(R.id.G2);
        G3 = findViewById(R.id.G3);
        G4 = findViewById(R.id.G4);
        G5 = findViewById(R.id.G5);
        G6 = findViewById(R.id.G6);
        G7 = findViewById(R.id.G7);
        G8 = findViewById(R.id.G8);
        G9 = findViewById(R.id.G9);
        G10 = findViewById(R.id.G10);

        H1 = findViewById(R.id.H1);
        H2 = findViewById(R.id.H2);
        H3 = findViewById(R.id.H3);
        H4 = findViewById(R.id.H4);
        H5 = findViewById(R.id.H5);
        H6 = findViewById(R.id.H6);
        H7 = findViewById(R.id.H7);
        H8 = findViewById(R.id.H8);
        H9 = findViewById(R.id.H9);
        H10 = findViewById(R.id.H10);

        I1 = findViewById(R.id.I1);
        I2 = findViewById(R.id.I2);
        I3 = findViewById(R.id.I3);
        I4 = findViewById(R.id.I4);
        I5 = findViewById(R.id.I5);
        I6 = findViewById(R.id.I6);
        I7 = findViewById(R.id.I7);
        I8 = findViewById(R.id.I8);
        I9 = findViewById(R.id.I9);
        I10 = findViewById(R.id.I10);

        J1 = findViewById(R.id.J1);
        J2 = findViewById(R.id.J2);
        J3 = findViewById(R.id.J3);
        J4 = findViewById(R.id.J4);
        J5 = findViewById(R.id.J5);
        J6 = findViewById(R.id.J6);
        J7 = findViewById(R.id.J7);
        J8 = findViewById(R.id.J8);
        J9 = findViewById(R.id.J9);
        J10 = findViewById(R.id.J10);
    }
    public void game(View view){}

    public void gameOne(View view){
        String temp = String.valueOf(view);
        System.out.println(temp);
        temp = checkId(temp);
        System.out.println(temp);
        boolean playermove = true;
        if (playermove) {
            //String temp = source.getId();
            switch (groundAI.ground_tab[groundAI.buttonAssignment(temp)]) {
                case 2:
                    //label1.setText("Nie mozna wybierac tego\n samego pola 2 razy");
                    break;
                case 1:
                    view.setBackgroundColor(Color.RED);
                    //label1.setText("Trafienie!");
                    groundAI.ground_tab[groundAI.buttonAssignment(temp)] = 2;
                    HP_AI--;
                    //progress_AI=progress_AI-0.1;
                    //progressBar_AI.setProgress((int) progress_AI);
                    break;
                case 0:
                    view.setBackgroundColor(Color.BLACK);
                    //label1.setText("Pudło!");
                    groundAI.ground_tab[groundAI.buttonAssignment(temp)] = 2;
                    playermove = false;
                    break;
            }

        }
        if(HP_AI<=0){
            Intent intent = new Intent(this, EndScreen.class);
            String message = "Wygrałeś";
            intent.putExtra(Intent.EXTRA_TEXT, message);
            startActivity(intent);
        }
        while (playermove == false&&HP_player>0) {
            int min = 1;
            int max = 100;
            int AI_move = (int) Math.floor(Math.random() * (AI_difficulty - min + 1) + min);
            if (AI_move >= 50||glitchCatcher()) {
                HP_player--;
                boolean flag_AI = true;
                while (flag_AI) {
                    int AI_shoot = (int) Math.floor(Math.random() * (max - min + 1) + min);
                    if (ground.ground_tab[AI_shoot] == 1) {
                        buttonArray(AI_shoot).setBackgroundColor(Color.RED);
                        //buttonArray(i).setBackgroundColor(water);
                        ground.ground_tab[AI_shoot]=2;
                        //progress_Player=progress_Player-0.1;
                        //progressBar_Player.setProgress((int) progress_Player);
                        flag_AI=false;
                    }
                }
            } else {
                boolean flag_AI = true;
                while (flag_AI) {
                    int AI_shoot = (int) Math.floor(Math.random() * (max - min + 1) + min);
                    if (ground.ground_tab[AI_shoot] == 0) {
                        buttonArray(AI_shoot).setBackgroundColor(Color.BLACK);
                        ground.ground_tab[AI_shoot]=2;
                        flag_AI=false;
                    }
                }
                playermove = true;
            }
        }
        if(HP_player<=0){
            Intent intent = new Intent(this, EndScreen.class);
            String message = "Przegrałeś";
            intent.putExtra(Intent.EXTRA_TEXT, message);
            startActivity(intent);
        }

    }

    private String checkId(String temp){
        String id="";
        char temp4 =temp.charAt(temp.length()-4);
        char temp3 =temp.charAt(temp.length()-3);
        char temp2=temp.charAt(temp.length()-2);
        //System.out.println(temp.charAt(temp.length()-4));
        //System.out.println(temp.charAt(temp.length()-3));
        //System.out.println(temp.charAt(temp.length()-2));
        id+=temp4;
        //System.out.println(id);
        id+=temp3;
        //System.out.println(id);
        id+=temp2;
        //System.out.println(id);
        if(temp4=='1'){
            id=temp.charAt(temp.length()-5)+id;
            //System.out.println(id+" Wykonal sie if");
        }
        return id;
    }

    private boolean glitchCatcher() {
        for(int i=1;i<ground.ground_tab.length;i++){
            if(ground.ground_tab[i]==0){
                return false;
            }
        }
        return true;
    }

    private Button buttonArray(int i) {
        switch(i){
            //case 0:return A1;
            case 1:return A1; case 11:return A2; case 21:return A3; case 31:return A4; case 41:return A5; case 51:return A6; case 61:return A7; case 71:return A8; case 81:return A9; case 91:return A10;

            case 2:return B1; case 12:return B2; case 22:return B3; case 32:return B4; case 42:return B5; case 52:return B6; case 62:return B7; case 72:return B8; case 82:return B9; case 92:return B10;

            case  3:return C1; case  13:return C2; case  23:return C3; case  33:return C4; case  43:return C5; case  53:return C6; case  63:return C7; case  73:return C8; case  83:return C9; case  93:return C10;

            case  4:return D1; case  14:return D2; case  24:return D3; case  34:return D4; case  44:return D5; case  54:return D6; case  64:return D7; case  74:return D8; case  84:return D9; case  94:return D10;

            case  5:return E1; case  15:return E2; case  25:return E3; case  35:return E4; case  45:return E5; case  55:return E6; case  65:return E7; case  75:return E8; case  85:return E9; case  95:return E10;

            case  6:return F1; case  16:return F2; case  26:return F3; case  36:return F4; case  46:return F5; case  56:return F6; case  66:return F7; case  76:return F8; case  86:return F9; case  96:return F10;

            case  7:return G1; case  17:return G2; case  27:return G3; case  37:return G4; case  47:return G5; case  57:return G6; case  67:return G7; case  77:return G8; case  87:return G9; case  97:return G10;

            case  8:return H1; case  18:return H2; case  28:return H3; case  38:return H4; case  48:return H5; case  58:return H6; case  68:return H7; case  78:return H8; case  88:return H9; case  98:return H10;

            case  9:return I1; case  19:return I2; case  29:return I3; case  39:return I4; case  49:return I5; case  59:return I6; case  69:return I7; case  79:return I8; case  89:return I9; case  99:return I10;

            case  10:return J1; case  20:return J2; case  30:return J3; case  40:return J4; case  50:return J5; case  60:return J6; case  70:return J7; case  80:return J8; case  90:return J9; case  100:return J10;
        }
        return null;
    }
}