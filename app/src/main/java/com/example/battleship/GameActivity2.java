package com.example.battleship;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

public class GameActivity2 extends AppCompatActivity {
    private static GameBoard tab;
    private ImageView ship1, ship2, ship3, ship4;
    private Button a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10;
    private Button d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10;
    private Button g1,g2,g3,g4,g5,g6,g7,g8,g9,g10,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10;
    private Button j1,j2,j3,j4,j5,j6,j7,j8,j9,j10;
    //ship1
    private Button aa1,aa2,aa3,aa4,aa5,aa6;
    //ship2
    private Button bb1,bb2,bb3;
    //ship3
    private Button cc1,cc2,cc3;
    //ship4
    private Button dd1,dd2,dd3,dd4;
    private int b;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game2);
        initFindViewById();
        tab = new GameBoard();
    }

    private Button buttonArray(int i) {
        switch(i){
            case  1:return a1; case 11:return a2; case 21:return a3; case 31: return a4; case 41:return a5; case 51:return a6; case 61:return a7; case 71:return a8; case 81:return a9; case 91:return a10;

            case  2:return b1; case 12:return b2; case 22:return b3; case 32:return b4; case 42:return b5; case 52:return b6; case 62:return b7; case 72:return b8; case 82:return b9; case 92:return b10;

            case  3:return c1; case  13:return c2; case  23:return c3; case  33:return c4; case  43:return c5; case  53:return c6; case  63:return c7; case  73:return c8; case  83:return c9; case  93:return c10;

            case  4:return d1; case  14:return d2; case  24:return d3; case  34:return d4; case  44:return d5; case  54:return d6; case  64:return d7; case  74:return d8; case  84:return d9; case  94:return d10;

            case  5:return e1; case  15:return e2; case  25:return e3; case  35:return e4; case  45:return e5; case  55:return e6; case  65:return e7; case  75:return e8; case  85:return e9; case  95:return e10;

            case  6:return f1; case  16:return f2; case  26:return f3; case  36:return f4; case  46:return f5; case  56:return f6; case  66:return f7; case  76:return f8; case  86:return f9; case  96:return f10;

            case  7:return g1; case  17:return g2; case  27:return g3; case  37:return g4; case  47:return g5; case  57:return g6; case  67:return g7; case  77:return g8; case  87:return g9; case  97:return g10;

            case  8:return h1; case  18:return h2; case  28:return h3; case  38:return h4; case  48:return h5; case  58:return h6; case  68:return h7; case  78:return h8; case  88:return h9; case  98:return h10;

            case  9:return i1; case  19:return i2; case  29:return i3; case  39:return i4; case  49:return i5; case  59:return i6; case  69:return i7; case  79:return i8; case  89:return i9; case  99:return i10;

            case  10:return j1; case  20:return j2; case  30:return j3; case  40:return j4; case  50:return j5; case  60:return j6; case  70:return j7; case  80:return j8; case  90:return j9; case  100:return j10;
        }
        return null;
    }
    public static GameBoard getBoard(){
        return tab;
    }


    public void check(View view){
        for(int i=1; i<101; i++)
        {
            assert buttonArray(i) != null;
            if (Color.BLUE == ((ColorDrawable)buttonArray(i).getBackground()).getColor()) {
                tab.ground_tab[i]=0;
            }
            else {
                assert buttonArray(i) != null;
                if (Color.RED == ((ColorDrawable)buttonArray(i).getBackground()).getColor()){
                        tab.ground_tab[i]=1;
                    }
            }
        }
        for(int i=1; i<101; i++){
            if (i%10 == 0){
                System.out.print(tab.ground_tab[i]+"\n");
            }
            else{
                System.out.print(tab.ground_tab[i]);
            }
        }
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    public final class MyTouchListener implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {

            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                view.startDrag(data, shadowBuilder, view, 0);
                view.setVisibility(View.VISIBLE);
                if (view.equals(ship1)) {
                    b = 1;
                }
                if (view.equals(ship2)) {
                    b = 2;
                }
                if (view.equals(ship3)) {
                    b = 3;
                }
                if (view.equals(ship4)) {
                    b = 4;
                }
                return true;
            } else {
                return false;
            }
        }
    }
    public boolean flag=true;


    int water = Color.BLUE;
    int ship = Color.RED;

    public void setPlace() {
        aa1.setBackgroundColor(ship);
        aa2.setBackgroundColor(ship);
        aa3.setBackgroundColor(ship);
        aa4.setBackgroundColor(ship);
        aa5.setBackgroundColor(ship);
        aa6.setBackgroundColor(ship);
        bb1.setBackgroundColor(ship);
        bb2.setBackgroundColor(ship);
        bb3.setBackgroundColor(ship);
        cc1.setBackgroundColor(ship);
        cc2.setBackgroundColor(ship);
        cc3.setBackgroundColor(ship);
        dd1.setBackgroundColor(ship);
        dd2.setBackgroundColor(ship);
        dd3.setBackgroundColor(ship);
        dd4.setBackgroundColor(ship);
    }

    public void reset() {
        for(int i=1; i<101; i++){
            buttonArray(i).setBackgroundColor(water);
        }
    }


    class MyDragListener implements View.OnDragListener {
        Drawable enterShape = ResourcesCompat.getDrawable(getResources(), R.drawable.water2, null);
        Drawable normalShape = ResourcesCompat.getDrawable(getResources(), R.drawable.water, null);





        void fun1(Button a1,Button a2,Button a3,Button a4,Button a5,Button a6) {
            aa1=a1;
            aa2=a2;
            aa3=a3;
            aa4=a4;
            aa5=a5;
            aa6=a6;


        }
        void fun2(Button a1,Button a2,Button a3) {
            bb1=a1;
            bb2=a2;
            bb3=a3;
        }
        void fun3(Button a1,Button a2,Button a3) {
            cc1=a1;
            cc2=a2;
            cc3=a3;
        }
        void fun4(Button a1,Button a2,Button a3,Button a4) {
            dd1=a1;
            dd2=a2;
            dd3=a3;
            dd4=a4;
        }

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    setPlace();
                    flag=true;

                    break;

                case DragEvent.ACTION_DRAG_ENTERED:

                    if (b == 1) {

                        if (v == a4 || v == a3) {
                            a4.setBackgroundColor(ship);
                            a1.setBackgroundColor(ship);
                            a2.setBackgroundColor(ship);
                            a3.setBackgroundColor(ship);
                            a5.setBackgroundColor(ship);
                            a6.setBackgroundColor(ship);
                            if (aa1==bb1||aa2==bb2||aa3==bb3) flag=false;
                            fun1(a1, a2, a3, a4, a5, a6);

                        }
                        if (v == b4 || v == b3) {
                            b4.setBackgroundColor(ship);
                            b1.setBackgroundColor(ship);
                            b2.setBackgroundColor(ship);
                            b3.setBackgroundColor(ship);
                            b5.setBackgroundColor(ship);
                            b6.setBackgroundColor(ship);
                            fun1(b1, b2, b3, b4, b5, b6);
                        }
                        if (v == c4 || v == c3) {
                            c4.setBackgroundColor(ship);
                            c1.setBackgroundColor(ship);
                            c2.setBackgroundColor(ship);
                            c3.setBackgroundColor(ship);
                            c5.setBackgroundColor(ship);
                            c6.setBackgroundColor(ship);
                            fun1(c1, c2, c3, c4, c5, c6);
                        }
                        if (v == d4 || v == d3) {
                            d4.setBackgroundColor(ship);
                            d1.setBackgroundColor(ship);
                            d2.setBackgroundColor(ship);
                            d3.setBackgroundColor(ship);
                            d5.setBackgroundColor(ship);
                            d6.setBackgroundColor(ship);
                            fun1(d1, d2, d3, d4, d5, d6);
                        }
                        if (v == e4 || v == e3) {
                            e4.setBackgroundColor(ship);
                            e1.setBackgroundColor(ship);
                            e2.setBackgroundColor(ship);
                            e3.setBackgroundColor(ship);
                            e5.setBackgroundColor(ship);
                            e6.setBackgroundColor(ship);
                            fun1(e1, e2, e3, e4, e5, e6);
                        }
                        if (v == f4 || v == f3) {
                            f4.setBackgroundColor(ship);
                            f1.setBackgroundColor(ship);
                            f2.setBackgroundColor(ship);
                            f3.setBackgroundColor(ship);
                            f5.setBackgroundColor(ship);
                            f6.setBackgroundColor(ship);
                            fun1(f1, f2, f3, f4, f5, f6);

                        }
                        if (v == g4 || v == g3) {
                            g4.setBackgroundColor(ship);
                            g1.setBackgroundColor(ship);
                            g2.setBackgroundColor(ship);
                            g3.setBackgroundColor(ship);
                            g5.setBackgroundColor(ship);
                            g6.setBackgroundColor(ship);
                            fun1(g1, g2, g3, g4, g5, g6);
                        }
                        if (v == h4 || v == h3) {
                            h4.setBackgroundColor(ship);
                            h1.setBackgroundColor(ship);
                            h2.setBackgroundColor(ship);
                            h3.setBackgroundColor(ship);
                            h5.setBackgroundColor(ship);
                            h6.setBackgroundColor(ship);
                            fun1(h1, h2, h3, h4, h5, h6);
                        }
                        if (v == i4 || v == i3) {
                            i4.setBackgroundColor(ship);
                            i1.setBackgroundColor(ship);
                            i2.setBackgroundColor(ship);
                            i3.setBackgroundColor(ship);
                            i5.setBackgroundColor(ship);
                            i6.setBackgroundColor(ship);
                            fun1(i1, i2, i3, i4, i5, i6);
                        }
                        if (v == j4 || v == j3) {
                            j4.setBackgroundColor(ship);
                            j1.setBackgroundColor(ship);
                            j2.setBackgroundColor(ship);
                            j3.setBackgroundColor(ship);
                            j5.setBackgroundColor(ship);
                            j6.setBackgroundColor(ship);
                            fun1(j1, j2, j3, j4, j5, j6);
                        }
                        if (v == a5) {
                            a4.setBackgroundColor(ship);
                            a2.setBackgroundColor(ship);
                            a3.setBackgroundColor(ship);
                            a5.setBackgroundColor(ship);
                            a6.setBackgroundColor(ship);
                            a7.setBackgroundColor(ship);
                            fun1(a2, a3, a4, a5, a6, a7);
                        }
                        if (v == b5) {
                            b4.setBackgroundColor(ship);
                            b7.setBackgroundColor(ship);
                            b2.setBackgroundColor(ship);
                            b3.setBackgroundColor(ship);
                            b5.setBackgroundColor(ship);
                            b6.setBackgroundColor(ship);
                            fun1(b2, b3, b4, b5, b6, b7);
                        }
                        if (v == c5) {
                            c4.setBackgroundColor(ship);
                            c7.setBackgroundColor(ship);
                            c2.setBackgroundColor(ship);
                            c3.setBackgroundColor(ship);
                            c5.setBackgroundColor(ship);
                            c6.setBackgroundColor(ship);
                            fun1(c2, c3, c4, c5, c6, c7);
                        }
                        if (v == d5) {
                            d4.setBackgroundColor(ship);
                            d7.setBackgroundColor(ship);
                            d2.setBackgroundColor(ship);
                            d3.setBackgroundColor(ship);
                            d5.setBackgroundColor(ship);
                            d6.setBackgroundColor(ship);
                            fun1(d2, d3, d4, d5, d6, d7);
                        }
                        if (v == e5) {
                            e4.setBackgroundColor(ship);
                            e7.setBackgroundColor(ship);
                            e2.setBackgroundColor(ship);
                            e3.setBackgroundColor(ship);
                            e5.setBackgroundColor(ship);
                            e6.setBackgroundColor(ship);
                            fun1(e2, e3, e4, e5, e6, e7);
                        }
                        if (v == f5) {
                            f4.setBackgroundColor(ship);
                            f7.setBackgroundColor(ship);
                            f2.setBackgroundColor(ship);
                            f3.setBackgroundColor(ship);
                            f5.setBackgroundColor(ship);
                            f6.setBackgroundColor(ship);
                            fun1(f2, f3, f4, f5, f6, f7);

                        }

                        if (v == g5) {
                            g4.setBackgroundColor(ship);
                            g7.setBackgroundColor(ship);
                            g2.setBackgroundColor(ship);
                            g3.setBackgroundColor(ship);
                            g5.setBackgroundColor(ship);
                            g6.setBackgroundColor(ship);
                            fun1(g2, g3, g4, g5, g6, g7);
                        }
                        if (v == h5) {
                            h4.setBackgroundColor(ship);
                            h7.setBackgroundColor(ship);
                            h2.setBackgroundColor(ship);
                            h3.setBackgroundColor(ship);
                            h5.setBackgroundColor(ship);
                            h6.setBackgroundColor(ship);
                            fun1(h2, h3, h4, h5, h6, h7);
                        }
                        if (v == i5) {
                            i4.setBackgroundColor(ship);
                            i7.setBackgroundColor(ship);
                            i2.setBackgroundColor(ship);
                            i3.setBackgroundColor(ship);
                            i5.setBackgroundColor(ship);
                            i6.setBackgroundColor(ship);
                            fun1(i2, i3, i4, i5, i6, i7);
                        }
                        if (v == j5) {
                            j4.setBackgroundColor(ship);
                            j7.setBackgroundColor(ship);
                            j2.setBackgroundColor(ship);
                            j3.setBackgroundColor(ship);
                            j5.setBackgroundColor(ship);
                            j6.setBackgroundColor(ship);
                            fun1(j2, j3, j4, j5, j6, j7);
                        }
                        if (v == a6) {
                            a4.setBackgroundColor(ship);
                            a7.setBackgroundColor(ship);
                            a8.setBackgroundColor(ship);
                            a3.setBackgroundColor(ship);
                            a5.setBackgroundColor(ship);
                            a6.setBackgroundColor(ship);
                            fun1(a3, j4, j5, j6, j7, j8);
                        }
                        if (v == b6) {
                            b4.setBackgroundColor(ship);
                            b7.setBackgroundColor(ship);
                            b8.setBackgroundColor(ship);
                            b3.setBackgroundColor(ship);
                            b5.setBackgroundColor(ship);
                            b6.setBackgroundColor(ship);
                            fun1(b3, b4, b5, b6, b7, b8);
                        }
                        if (v == c6) {
                            c4.setBackgroundColor(ship);
                            c7.setBackgroundColor(ship);
                            c8.setBackgroundColor(ship);
                            c3.setBackgroundColor(ship);
                            c5.setBackgroundColor(ship);
                            c6.setBackgroundColor(ship);
                            fun1(c3, c4, c5, c6, c7, c8);
                        }
                        if (v == d6) {
                            d4.setBackgroundColor(ship);
                            d7.setBackgroundColor(ship);
                            d8.setBackgroundColor(ship);
                            d3.setBackgroundColor(ship);
                            d5.setBackgroundColor(ship);
                            d6.setBackgroundColor(ship);
                            fun1(d3, d4, d5, d6, d7, d8);
                        }
                        if (v == e6) {
                            e4.setBackgroundColor(ship);
                            e7.setBackgroundColor(ship);
                            e8.setBackgroundColor(ship);
                            e3.setBackgroundColor(ship);
                            e5.setBackgroundColor(ship);
                            e6.setBackgroundColor(ship);
                            fun1(e3, e4, e5, e6, e7, e8);
                        }
                        if (v == f6) {
                            f4.setBackgroundColor(ship);
                            f7.setBackgroundColor(ship);
                            f8.setBackgroundColor(ship);
                            f3.setBackgroundColor(ship);
                            f5.setBackgroundColor(ship);
                            f6.setBackgroundColor(ship);
                            fun1(f3, f4, f5, f6, f7, f8);
                        }
                        if (v == g6) {
                            g4.setBackgroundColor(ship);
                            g7.setBackgroundColor(ship);
                            g8.setBackgroundColor(ship);
                            g3.setBackgroundColor(ship);
                            g5.setBackgroundColor(ship);
                            g6.setBackgroundColor(ship);
                            fun1(g3, g4, g5, g6, g7, g8);
                        }
                        if (v == h6) {
                            h4.setBackgroundColor(ship);
                            h7.setBackgroundColor(ship);
                            h8.setBackgroundColor(ship);
                            h3.setBackgroundColor(ship);
                            h5.setBackgroundColor(ship);
                            h6.setBackgroundColor(ship);
                            fun1(h3, h4, h5, h6, h7, h8);
                        }
                        if (v == i6) {
                            i4.setBackgroundColor(ship);
                            i7.setBackgroundColor(ship);
                            i8.setBackgroundColor(ship);
                            i3.setBackgroundColor(ship);
                            i5.setBackgroundColor(ship);
                            i6.setBackgroundColor(ship);
                            fun1(i3, i4, i5, i6, i7, i8);
                        }
                        if (v == j6) {
                            j4.setBackgroundColor(ship);
                            j7.setBackgroundColor(ship);
                            j8.setBackgroundColor(ship);
                            j3.setBackgroundColor(ship);
                            j5.setBackgroundColor(ship);
                            j6.setBackgroundColor(ship);
                            fun1(j3, j4, j5, j6, j7, j8);
                        }
                        if (v == a7) {
                            a4.setBackgroundColor(ship);
                            a7.setBackgroundColor(ship);
                            a8.setBackgroundColor(ship);
                            a9.setBackgroundColor(ship);
                            a5.setBackgroundColor(ship);
                            a6.setBackgroundColor(ship);
                            fun1(a4, a5, a6, a7, a8, a9);
                        }
                        if (v == b7) {
                            b4.setBackgroundColor(ship);
                            b7.setBackgroundColor(ship);
                            b8.setBackgroundColor(ship);
                            b9.setBackgroundColor(ship);
                            b5.setBackgroundColor(ship);
                            b6.setBackgroundColor(ship);
                            fun1(b4, b5, b6, b7, b8, b9);
                        }
                        if (v == c7) {
                            c4.setBackgroundColor(ship);
                            c7.setBackgroundColor(ship);
                            c8.setBackgroundColor(ship);
                            c9.setBackgroundColor(ship);
                            c5.setBackgroundColor(ship);
                            c6.setBackgroundColor(ship);
                            fun1(c4, c5, c6, c7, c8, c9);
                        }
                        if (v == d7) {
                            d4.setBackgroundColor(ship);
                            d7.setBackgroundColor(ship);
                            d8.setBackgroundColor(ship);
                            d9.setBackgroundColor(ship);
                            d5.setBackgroundColor(ship);
                            d6.setBackgroundColor(ship);
                            fun1(d4, d5, d6, d7, d8, d9);
                        }
                        if (v == e7) {
                            e4.setBackgroundColor(ship);
                            e7.setBackgroundColor(ship);
                            e8.setBackgroundColor(ship);
                            e9.setBackgroundColor(ship);
                            e5.setBackgroundColor(ship);
                            e6.setBackgroundColor(ship);
                            fun1(e4, e5, e6, e7, e8, e9);
                        }
                        if (v == f7) {
                            f4.setBackgroundColor(ship);
                            f7.setBackgroundColor(ship);
                            f8.setBackgroundColor(ship);
                            f9.setBackgroundColor(ship);
                            f5.setBackgroundColor(ship);
                            f6.setBackgroundColor(ship);
                            fun1(f4, f5, f6, f7, f8, f9);
                        }
                        if (v == g7) {
                            g4.setBackgroundColor(ship);
                            g7.setBackgroundColor(ship);
                            g8.setBackgroundColor(ship);
                            g9.setBackgroundColor(ship);
                            g5.setBackgroundColor(ship);
                            g6.setBackgroundColor(ship);
                            fun1(g4, g5, g6, g7, g8, g9);
                        }
                        if (v == h7) {
                            h4.setBackgroundColor(ship);
                            h7.setBackgroundColor(ship);
                            h8.setBackgroundColor(ship);
                            h9.setBackgroundColor(ship);
                            h5.setBackgroundColor(ship);
                            h6.setBackgroundColor(ship);
                            fun1(h4, h5, h6, h7, h8, h9);
                        }
                        if (v == i7) {
                            i4.setBackgroundColor(ship);
                            i7.setBackgroundColor(ship);
                            i8.setBackgroundColor(ship);
                            i9.setBackgroundColor(ship);
                            i5.setBackgroundColor(ship);
                            i6.setBackgroundColor(ship);
                            fun1(i4, i5, i6, i7, i8, i9);
                        }
                        if (v == j7) {
                            j4.setBackgroundColor(ship);
                            j7.setBackgroundColor(ship);
                            j8.setBackgroundColor(ship);
                            j9.setBackgroundColor(ship);
                            j5.setBackgroundColor(ship);
                            j6.setBackgroundColor(ship);
                            fun1(j4, j5, j6, j7, j8, j9);
                        }
                        if (v == a8) {
                            a10.setBackgroundColor(ship);
                            a7.setBackgroundColor(ship);
                            a8.setBackgroundColor(ship);
                            a9.setBackgroundColor(ship);
                            a5.setBackgroundColor(ship);
                            a6.setBackgroundColor(ship);
                            fun1(a5, a6, a7, a8, a9, a10);
                        }
                        if (v == b8) {
                            b9.setBackgroundColor(ship);
                            b7.setBackgroundColor(ship);
                            b8.setBackgroundColor(ship);
                            b10.setBackgroundColor(ship);
                            b5.setBackgroundColor(ship);
                            b6.setBackgroundColor(ship);
                            fun1(b5, b6, b7, b8, b9, b10);
                        }
                        if (v == c8) {
                            c10.setBackgroundColor(ship);
                            c7.setBackgroundColor(ship);
                            c8.setBackgroundColor(ship);
                            c9.setBackgroundColor(ship);
                            c5.setBackgroundColor(ship);
                            c6.setBackgroundColor(ship);
                            fun1(c5, c6, c7, c8, c9, c10);
                        }
                        if (v == d8) {
                            d10.setBackgroundColor(ship);
                            d7.setBackgroundColor(ship);
                            d8.setBackgroundColor(ship);
                            d9.setBackgroundColor(ship);
                            d5.setBackgroundColor(ship);
                            d6.setBackgroundColor(ship);
                            fun1(d5, d6, d7, d8, d9, d10);
                        }
                        if (v == e8) {
                            e10.setBackgroundColor(ship);
                            e7.setBackgroundColor(ship);
                            e8.setBackgroundColor(ship);
                            e9.setBackgroundColor(ship);
                            e5.setBackgroundColor(ship);
                            e6.setBackgroundColor(ship);
                            fun1(e5, e6, e7, e8, e9, e10);
                        }
                        if (v == f8) {
                            f10.setBackgroundColor(ship);
                            f7.setBackgroundColor(ship);
                            f8.setBackgroundColor(ship);
                            f9.setBackgroundColor(ship);
                            f5.setBackgroundColor(ship);
                            f6.setBackgroundColor(ship);
                            fun1(f5, f6, f7, f8, f9, f10);

                        }
                        if (v == g8) {
                            g10.setBackgroundColor(ship);
                            g7.setBackgroundColor(ship);
                            g8.setBackgroundColor(ship);
                            g9.setBackgroundColor(ship);
                            g5.setBackgroundColor(ship);
                            g6.setBackgroundColor(ship);
                            fun1(g5, g6, g7, g8, g9, g10);
                        }
                        if (v == h8) {
                            h10.setBackgroundColor(ship);
                            h7.setBackgroundColor(ship);
                            h8.setBackgroundColor(ship);
                            h9.setBackgroundColor(ship);
                            h5.setBackgroundColor(ship);
                            h6.setBackgroundColor(ship);
                            fun1(h5, h6, h7, h8, h9, h10);
                        }
                        if (v == i8) {
                            i10.setBackgroundColor(ship);
                            i7.setBackgroundColor(ship);
                            i8.setBackgroundColor(ship);
                            i9.setBackgroundColor(ship);
                            i5.setBackgroundColor(ship);
                            i6.setBackgroundColor(ship);
                            fun1(i5, i6, i7, i8, i9, i10);
                        }
                        if (v == j8) {
                            j10.setBackgroundColor(ship);
                            j7.setBackgroundColor(ship);
                            j8.setBackgroundColor(ship);
                            j9.setBackgroundColor(ship);
                            j5.setBackgroundColor(ship);
                            j6.setBackgroundColor(ship);
                            fun1(j5, j6, j7, j8, j9, j10);
                        }
                    }
                    if (b == 2 || b == 3) {
                        if (v == a2) {
                            a2.setBackgroundColor(ship);
                            a1.setBackgroundColor(ship);
                            a3.setBackgroundColor(ship);
                            if (b == 2) fun2(a1, a2, a3);
                            else fun3(a1, a2, a3);
                        }
                        if (v == b2) {
                            v.setBackgroundColor(ship);
                            b1.setBackgroundColor(ship);
                            b3.setBackgroundColor(ship);
                            if (b == 2) fun2(b1, b2, b3);
                            else fun3(b1, b2, b3);
                        }
                        if (v == c2) {
                            v.setBackgroundColor(ship);
                            c1.setBackgroundColor(ship);
                            c3.setBackgroundColor(ship);
                            if (b == 2) fun2(c1, c2, c3);
                            else fun3(c1, c2, c3);
                        }
                        if (v == d2) {
                            v.setBackgroundColor(ship);
                            d1.setBackgroundColor(ship);
                            d3.setBackgroundColor(ship);
                            if (b == 2) fun2(d1, d2, d3);
                            else fun3(d1, d2, d3);
                        }
                        if (v == e2) {
                            v.setBackgroundColor(ship);
                            e1.setBackgroundColor(ship);
                            e3.setBackgroundColor(ship);
                            if (b == 2) fun2(e1, e2, e3);
                            else fun3(e1, e2, e3);
                        }
                        if (v == f2) {
                            v.setBackgroundColor(ship);
                            f1.setBackgroundColor(ship);
                            f3.setBackgroundColor(ship);
                            if (b == 2) fun2(f1, f2, f3);
                            else fun3(f1, f2, f3);
                        }
                        if (v == g2) {
                            v.setBackgroundColor(ship);
                            g1.setBackgroundColor(ship);
                            g3.setBackgroundColor(ship);
                            if (b == 2) fun2(g1, g2, g3);
                            else fun3(g1, g2, g3);
                        }
                        if (v == h2) {
                            v.setBackgroundColor(ship);
                            h1.setBackgroundColor(ship);
                            h3.setBackgroundColor(ship);
                            if (b == 2) fun2(h1, h2, h3);
                            else fun3(h1, h2, h3);
                        }
                        if (v == i2) {
                            v.setBackgroundColor(ship);
                            i1.setBackgroundColor(ship);
                            i3.setBackgroundColor(ship);
                            if (b == 2) fun2(i1, i2, i3);
                            else fun3(i1, i2, i3);
                        }
                        if (v == j2) {
                            v.setBackgroundColor(ship);
                            j1.setBackgroundColor(ship);
                            j3.setBackgroundColor(ship);
                            if (b == 2) fun2(j1, j2, j3);
                            else fun3(j1, j2, j3);
                        }
                        if (v == a3) {
                            v.setBackgroundColor(ship);
                            a2.setBackgroundColor(ship);
                            a4.setBackgroundColor(ship);
                            if (b == 2) fun2(a2, a3, a4);
                            else fun3(a2, a3, a4);
                        }
                        if (v == b3) {
                            v.setBackgroundColor(ship);
                            b2.setBackgroundColor(ship);
                            b4.setBackgroundColor(ship);
                            if (b == 2) fun2(b2, b3, b4);
                            else fun3(b2, b3, b4);
                        }
                        if (v == c3) {
                            v.setBackgroundColor(ship);
                            c2.setBackgroundColor(ship);
                            c4.setBackgroundColor(ship);
                            if (b == 2) fun2(c2, c3, c4);
                            else fun3(c2, c3, c4);
                        }
                        if (v == d3) {
                            v.setBackgroundColor(ship);
                            d2.setBackgroundColor(ship);
                            d4.setBackgroundColor(ship);
                            if (b == 2) fun2(d2, d3, d4);
                            else fun3(d2, d3, d4);
                        }
                        if (v == e3) {
                            v.setBackgroundColor(ship);
                            e2.setBackgroundColor(ship);
                            e4.setBackgroundColor(ship);
                            if (b == 2) fun2(e2, e3, e4);
                            else fun3(e2, e3, e4);
                        }
                        if (v == f3) {
                            v.setBackgroundColor(ship);
                            f2.setBackgroundColor(ship);
                            f4.setBackgroundColor(ship);
                            if (b == 2) fun2(f2, f3, f4);
                            else fun3(f2, f3, f4);
                        }
                        if (v == g3) {
                            v.setBackgroundColor(ship);
                            g2.setBackgroundColor(ship);
                            g4.setBackgroundColor(ship);
                            if (b == 2) fun2(g2, g3, g4);
                            else fun3(g2, g3, g4);
                        }
                        if (v == h3) {
                            v.setBackgroundColor(ship);
                            h2.setBackgroundColor(ship);
                            h4.setBackgroundColor(ship);
                            if (b == 2) fun2(h2, h3, h4);
                            else fun3(h2, h3, h4);
                        }
                        if (v == i3) {
                            v.setBackgroundColor(ship);
                            i2.setBackgroundColor(ship);
                            i4.setBackgroundColor(ship);
                            if (b == 2) fun2(i2, i3, i4);
                            else fun3(i2, i3, i4);
                        }
                        if (v == j3) {
                            v.setBackgroundColor(ship);
                            j2.setBackgroundColor(ship);
                            j4.setBackgroundColor(ship);
                            if (b == 2) fun2(j2, j3, j4);
                            else fun3(j2, j3, j4);
                        }
                        if (v == a4) {
                            v.setBackgroundColor(ship);
                            a3.setBackgroundColor(ship);
                            a5.setBackgroundColor(ship);
                            if (b == 2) fun2(a3, a4, a5);
                            else fun3(a3, a4, a5);
                        }
                        if (v == b4) {
                            v.setBackgroundColor(ship);
                            b3.setBackgroundColor(ship);
                            b5.setBackgroundColor(ship);
                            if (b == 2) fun2(b3, b4, b5);
                            else fun3(b3, b4, b5);
                        }
                        if (v == c4) {
                            v.setBackgroundColor(ship);
                            c3.setBackgroundColor(ship);
                            c5.setBackgroundColor(ship);
                            if (b == 2) fun2(c3, c4, c5);
                            else fun3(c3, c4, c5);
                        }
                        if (v == d4) {
                            v.setBackgroundColor(ship);
                            d3.setBackgroundColor(ship);
                            d5.setBackgroundColor(ship);
                            if (b == 2) fun2(d3, d4, d5);
                            else fun3(d3, d4, d5);
                        }
                        if (v == e4) {
                            v.setBackgroundColor(ship);
                            e3.setBackgroundColor(ship);
                            e5.setBackgroundColor(ship);
                            if (b == 2) fun2(e3, e4, e5);
                            else fun3(e3, e4, e5);
                        }
                        if (v == f4) {
                            v.setBackgroundColor(ship);
                            f3.setBackgroundColor(ship);
                            f5.setBackgroundColor(ship);
                            if (b == 2) fun2(f3, f4, f5);
                            else fun3(f3, f4, f5);
                        }

                        if (v == g4) {
                            v.setBackgroundColor(ship);
                            g3.setBackgroundColor(ship);
                            g5.setBackgroundColor(ship);
                            if (b == 2) fun2(g3, g4, g5);
                            else fun3(g3, g4, g5);
                        }
                        if (v == h4) {
                            v.setBackgroundColor(ship);
                            h3.setBackgroundColor(ship);
                            h5.setBackgroundColor(ship);
                            if (b == 2) fun2(h3, h4, h5);
                            else fun3(h3, h4, h5);
                        }
                        if (v == i4) {
                            v.setBackgroundColor(ship);
                            i3.setBackgroundColor(ship);
                            i5.setBackgroundColor(ship);
                            if (b == 2) fun2(i3, i4, i5);
                            else fun3(i3, i4, i5);
                        }
                        if (v == j4) {
                            v.setBackgroundColor(ship);
                            j3.setBackgroundColor(ship);
                            j5.setBackgroundColor(ship);
                            if (b == 2) fun2(j3, j4, j5);
                            else fun3(j3, j4, j5);
                        }
                        if (v == a5) {
                            v.setBackgroundColor(ship);
                            a4.setBackgroundColor(ship);
                            a6.setBackgroundColor(ship);
                            if (b == 2) fun2(a4, a5, a6);
                            else fun3(a4, a5, a6);

                        }
                        if (v == b5) {
                            v.setBackgroundColor(ship);
                            b4.setBackgroundColor(ship);
                            b6.setBackgroundColor(ship);
                            if (b == 2) fun2(b4, b5, b6);
                            else fun3(b4, b5, b6);
                        }
                        if (v == c5) {
                            v.setBackgroundColor(ship);
                            c4.setBackgroundColor(ship);
                            c6.setBackgroundColor(ship);
                            if (b == 2) fun2(c4, c5, c6);
                            else fun3(c4, c5, c6);
                        }
                        if (v == d5) {
                            v.setBackgroundColor(ship);
                            d4.setBackgroundColor(ship);
                            d6.setBackgroundColor(ship);
                            if (b == 2) fun2(d4, d5, d6);
                            else fun3(d4, d5, d6);
                        }
                        if (v == e5) {
                            v.setBackgroundColor(ship);
                            e4.setBackgroundColor(ship);
                            e6.setBackgroundColor(ship);
                            if (b == 2) fun2(e4, e5, e6);
                            else fun3(e4, e5, e6);
                        }
                        if (v == f5) {
                            v.setBackgroundColor(ship);
                            f4.setBackgroundColor(ship);
                            f6.setBackgroundColor(ship);
                            if (b == 2) fun2(f4, f5, f6);
                            else fun3(f4, f5, f6);
                        }

                        if (v == g5) {
                            v.setBackgroundColor(ship);
                            g4.setBackgroundColor(ship);
                            g6.setBackgroundColor(ship);
                            if (b == 2) fun2(g4, g5, g6);
                            else fun3(g4, g5, g6);
                        }
                        if (v == h5) {
                            v.setBackgroundColor(ship);
                            h4.setBackgroundColor(ship);
                            h6.setBackgroundColor(ship);
                            if (b == 2) fun2(h4, h5, h6);
                            else fun3(h4, h5, h6);
                        }
                        if (v == i5) {
                            v.setBackgroundColor(ship);
                            i4.setBackgroundColor(ship);
                            i6.setBackgroundColor(ship);
                            if (b == 2) fun2(i4, i5, i6);
                            else fun3(i4, i5, i6);
                        }
                        if (v == j5) {
                            v.setBackgroundColor(ship);
                            j4.setBackgroundColor(ship);
                            j6.setBackgroundColor(ship);
                            if (b == 2) fun2(j4, j5, j6);
                            else fun3(j4, j5, j6);
                        }
                        if (v == a6) {
                            v.setBackgroundColor(ship);
                            a5.setBackgroundColor(ship);
                            a7.setBackgroundColor(ship);
                            if (b == 2) fun2(a5, a6, a7);
                            else fun3(a5, a6, a7);
                        }
                        if (v == b6) {
                            v.setBackgroundColor(ship);
                            b5.setBackgroundColor(ship);
                            b7.setBackgroundColor(ship);
                            if (b == 2) fun2(b5, b6, b7);
                            else fun3(b5, b6, b7);
                        }
                        if (v == c6) {
                            v.setBackgroundColor(ship);
                            c5.setBackgroundColor(ship);
                            c7.setBackgroundColor(ship);
                            if (b == 2) fun2(c5, c6, c7);
                            else fun3(c5, c6, c7);
                        }
                        if (v == d6) {
                            v.setBackgroundColor(ship);
                            d5.setBackgroundColor(ship);
                            d7.setBackgroundColor(ship);
                            if (b == 2) fun2(d5, d6, d7);
                            else fun3(d5, d6, d7);
                        }
                        if (v == e6) {
                            v.setBackgroundColor(ship);
                            e5.setBackgroundColor(ship);
                            e7.setBackgroundColor(ship);
                            if (b == 2) fun2(e5, e6, e7);
                            else fun3(e5, e6, e7);
                        }
                        if (v == f6) {
                            v.setBackgroundColor(ship);
                            f5.setBackgroundColor(ship);
                            f7.setBackgroundColor(ship);
                            if (b == 2) fun2(f5, f6, f7);
                            else fun3(f5, f6, f7);
                        }

                        if (v == g6) {
                            v.setBackgroundColor(ship);
                            g5.setBackgroundColor(ship);
                            g7.setBackgroundColor(ship);
                            if (b == 2) fun2(g5, g6, g7);
                            else fun3(g5, g6, g7);
                        }
                        if (v == h6) {
                            v.setBackgroundColor(ship);
                            h5.setBackgroundColor(ship);
                            h7.setBackgroundColor(ship);
                            if (b == 2) fun2(h5, h6, h7);
                            else fun3(h5, h6, h7);
                        }
                        if (v == i6) {
                            v.setBackgroundColor(ship);
                            i5.setBackgroundColor(ship);
                            i7.setBackgroundColor(ship);
                            if (b == 2) fun2(i5, i6, i7);
                            else fun3(i5, i6, i7);
                        }
                        if (v == j6) {
                            v.setBackgroundColor(ship);
                            j5.setBackgroundColor(ship);
                            j7.setBackgroundColor(ship);
                            if (b == 2) fun2(j5, j6, j7);
                            else fun3(j5, j6, j7);
                        }

                        if (v == a7) {
                            v.setBackgroundColor(ship);
                            a6.setBackgroundColor(ship);
                            a8.setBackgroundColor(ship);
                            if (b == 2) fun2(a6, a7, a8);
                            else fun3(a6, a7, a8);
                        }
                        if (v == b7) {
                            v.setBackgroundColor(ship);
                            b6.setBackgroundColor(ship);
                            b8.setBackgroundColor(ship);
                            if (b == 2) fun2(b6, b7, b8);
                            else fun3(b6, b7, b8);
                        }
                        if (v == c7) {
                            v.setBackgroundColor(ship);
                            c6.setBackgroundColor(ship);
                            c8.setBackgroundColor(ship);
                            if (b == 2) fun2(c6, c7, c8);
                            else fun3(c6, c7, c8);
                        }
                        if (v == d7) {
                            v.setBackgroundColor(ship);
                            d6.setBackgroundColor(ship);
                            d8.setBackgroundColor(ship);
                            if (b == 2) fun2(d6, d7, d8);
                            else fun3(d6, d7, d8);
                        }
                        if (v == e7) {
                            v.setBackgroundColor(ship);
                            e6.setBackgroundColor(ship);
                            e8.setBackgroundColor(ship);
                            if (b == 2) fun2(e6, e7, e8);
                            else fun3(e6, e7, e8);
                        }
                        if (v == f7) {
                            v.setBackgroundColor(ship);
                            f6.setBackgroundColor(ship);
                            f8.setBackgroundColor(ship);
                            if (b == 2) fun2(f6, f7, f8);
                            else fun3(f6, f7, f8);
                        }

                        if (v == g7) {
                            v.setBackgroundColor(ship);
                            g6.setBackgroundColor(ship);
                            g8.setBackgroundColor(ship);
                            if (b == 2) fun2(g6, g7, g8);
                            else fun3(g6, g7, g8);
                        }
                        if (v == h7) {
                            v.setBackgroundColor(ship);
                            h6.setBackgroundColor(ship);
                            h8.setBackgroundColor(ship);
                            if (b == 2) fun2(h6, h7, h8);
                            else fun3(h6, h7, h8);
                        }
                        if (v == i7) {
                            v.setBackgroundColor(ship);
                            i6.setBackgroundColor(ship);
                            i8.setBackgroundColor(ship);
                            if (b == 2) fun2(i6, i7, i8);
                            else fun3(i6, i7, i8);
                        }
                        if (v == j7) {
                            v.setBackgroundColor(ship);
                            j6.setBackgroundColor(ship);
                            j8.setBackgroundColor(ship);
                            if (b == 2) fun2(j6, j7, j8);
                            else fun3(j6, j7, j8);
                        }
                        if (v == a8) {
                            v.setBackgroundColor(ship);
                            a7.setBackgroundColor(ship);
                            a9.setBackgroundColor(ship);
                            if (b == 2) fun2(a7, a8, a9);
                            else fun3(a7, a8, a9);
                        }
                        if (v == b8) {
                            v.setBackgroundColor(ship);
                            b7.setBackgroundColor(ship);
                            b9.setBackgroundColor(ship);
                            if (b == 2) fun2(b7, b8, b9);
                            else fun3(b7, b8, b9);
                        }
                        if (v == c8) {
                            v.setBackgroundColor(ship);
                            c7.setBackgroundColor(ship);
                            c9.setBackgroundColor(ship);
                            if (b == 2) fun2(c7, c8, c9);
                            else fun3(c7, c8, c9);
                        }
                        if (v == d8) {
                            v.setBackgroundColor(ship);
                            d7.setBackgroundColor(ship);
                            d9.setBackgroundColor(ship);
                            if (b == 2) fun2(d7, d8, d9);
                            else fun3(d7, d8, d9);
                        }
                        if (v == e8) {
                            v.setBackgroundColor(ship);
                            e7.setBackgroundColor(ship);
                            e9.setBackgroundColor(ship);
                            if (b == 2) fun2(e7, e8, e9);
                            else fun3(e7, e8, e9);
                        }
                        if (v == f8) {
                            v.setBackgroundColor(ship);
                            f7.setBackgroundColor(ship);
                            f9.setBackgroundColor(ship);
                            if (b == 2) fun2(f7, f8, f9);
                            else fun3(f7, f8, f9);
                        }
                        if (v == g8) {
                            v.setBackgroundColor(ship);
                            g7.setBackgroundColor(ship);
                            g9.setBackgroundColor(ship);
                            if (b == 2) fun2(g7, g8, g9);
                            else fun3(g7, g8, g9);
                        }
                        if (v == h8) {
                            v.setBackgroundColor(ship);
                            h7.setBackgroundColor(ship);
                            h9.setBackgroundColor(ship);
                            if (b == 2) fun2(h7, h8, h9);
                            else fun3(h7, h8, h9);
                        }
                        if (v == i8) {
                            v.setBackgroundColor(ship);
                            i7.setBackgroundColor(ship);
                            i9.setBackgroundColor(ship);
                            if (b == 2) fun2(i7, i8, i9);
                            else fun3(i7, i8, i9);
                        }
                        if (v == j8) {
                            v.setBackgroundColor(ship);
                            j7.setBackgroundColor(ship);
                            j9.setBackgroundColor(ship);
                            if (b == 2) fun2(j7, j8, j9);
                            else fun3(j7, j8, j9);
                        }

                        if (v == a9) {
                            v.setBackgroundColor(ship);
                            a8.setBackgroundColor(ship);
                            a10.setBackgroundColor(ship);
                            if (b == 2) fun2(a8, a9, a10);
                            else fun3(a8, a9, a10);
                        }
                        if (v == b9) {
                            v.setBackgroundColor(ship);
                            b8.setBackgroundColor(ship);
                            b10.setBackgroundColor(ship);
                            if (b == 2) fun2(b8, b9, b10);
                            else fun3(b8, b9, b10);
                        }
                        if (v == c9) {
                            v.setBackgroundColor(ship);
                            c8.setBackgroundColor(ship);
                            c10.setBackgroundColor(ship);
                            if (b == 2) fun2(c8, c9, c10);
                            else fun3(c8, c9, c10);
                        }
                        if (v == d9) {
                            v.setBackgroundColor(ship);
                            d8.setBackgroundColor(ship);
                            d10.setBackgroundColor(ship);
                            if (b == 2) fun2(d8, d9, d10);
                            else fun3(d8, d9, d10);
                        }
                        if (v == e9) {
                            v.setBackgroundColor(ship);
                            e8.setBackgroundColor(ship);
                            e10.setBackgroundColor(ship);
                            if (b == 2) fun2(e8, e9, e10);
                            else fun3(e8, e9, e10);
                        }
                        if (v == f9) {
                            v.setBackgroundColor(ship);
                            f8.setBackgroundColor(ship);
                            f10.setBackgroundColor(ship);
                            if (b == 2) fun2(f8, f9, f10);
                            else fun3(f8, f9, f10);
                        }
                        if (v == g9) {
                            v.setBackgroundColor(ship);
                            g8.setBackgroundColor(ship);
                            g10.setBackgroundColor(ship);
                            if (b == 2) fun2(g8, g9, g10);
                            else fun3(g8, g9, g10);
                        }
                        if (v == h9) {
                            v.setBackgroundColor(ship);
                            h8.setBackgroundColor(ship);
                            h10.setBackgroundColor(ship);
                            if (b == 2) fun2(h8, h9, h10);
                            else fun3(h8, h9, h10);
                        }
                        if (v == i9) {
                            v.setBackgroundColor(ship);
                            i8.setBackgroundColor(ship);
                            i10.setBackgroundColor(ship);
                            if (b == 2) fun2(i8, i9, i10);
                            else fun3(i8, i9, i10);
                        }
                        if (v == j9) {
                            v.setBackgroundColor(ship);
                            j8.setBackgroundColor(ship);
                            j10.setBackgroundColor(ship);
                            if (b == 2) fun2(j8, j9, j10);
                            else fun3(j8, j9, j10);
                        }
                    }
                    if (b == 4) {

                        if (v == a3) {
                            v.setBackgroundColor(ship);
                            a2.setBackgroundColor(ship);
                            a4.setBackgroundColor(ship);
                            a1.setBackgroundColor(ship);
                            fun4(a1, a2, a3, a4);
                        }
                        if (v == b3) {
                            v.setBackgroundColor(ship);
                            b2.setBackgroundColor(ship);
                            b4.setBackgroundColor(ship);
                            b1.setBackgroundColor(ship);
                            fun4(b1, b2, b3, b4);
                        }
                        if (v == c3) {
                            v.setBackgroundColor(ship);
                            c2.setBackgroundColor(ship);
                            c4.setBackgroundColor(ship);
                            c1.setBackgroundColor(ship);
                            fun4(c1, c2, c3, c4);
                        }
                        if (v == d3) {
                            v.setBackgroundColor(ship);
                            d2.setBackgroundColor(ship);
                            d4.setBackgroundColor(ship);
                            d1.setBackgroundColor(ship);
                            fun4(d1, d2, d3, d4);
                        }
                        if (v == e3) {
                            v.setBackgroundColor(ship);
                            e2.setBackgroundColor(ship);
                            e4.setBackgroundColor(ship);
                            e1.setBackgroundColor(ship);
                            fun4(e1, e2, e3, e4);
                        }
                        if (v == f3) {
                            v.setBackgroundColor(ship);
                            f2.setBackgroundColor(ship);
                            f4.setBackgroundColor(ship);
                            f1.setBackgroundColor(ship);
                            fun4(f1, f2, f3, f4);
                        }

                        if (v == g3) {
                            v.setBackgroundColor(ship);
                            g2.setBackgroundColor(ship);
                            g4.setBackgroundColor(ship);
                            g1.setBackgroundColor(ship);
                            fun4(g1, g2, g3, g4);
                        }
                        if (v == h3) {
                            v.setBackgroundColor(ship);
                            h2.setBackgroundColor(ship);
                            h4.setBackgroundColor(ship);
                            h1.setBackgroundColor(ship);
                            fun4(h1, h2, h3, h4);
                        }
                        if (v == i3) {
                            v.setBackgroundColor(ship);
                            i2.setBackgroundColor(ship);
                            i4.setBackgroundColor(ship);
                            i1.setBackgroundColor(ship);
                            fun4(i1, i2, i3, i4);
                        }
                        if (v == j3) {
                            v.setBackgroundColor(ship);
                            j2.setBackgroundColor(ship);
                            j4.setBackgroundColor(ship);
                            j1.setBackgroundColor(ship);
                            fun4(j1, j2, j3, j4);
                        }
                        if (v == a4) {
                            v.setBackgroundColor(ship);
                            a2.setBackgroundColor(ship);
                            a3.setBackgroundColor(ship);
                            a5.setBackgroundColor(ship);
                            fun4(a2, a3, a4, a5);
                        }
                        if (v == b4) {
                            v.setBackgroundColor(ship);
                            b2.setBackgroundColor(ship);
                            b3.setBackgroundColor(ship);
                            b5.setBackgroundColor(ship);
                            fun4(b2, b3, b4, b5);
                        }
                        if (v == c4) {
                            v.setBackgroundColor(ship);
                            c2.setBackgroundColor(ship);
                            c3.setBackgroundColor(ship);
                            c5.setBackgroundColor(ship);
                            fun4(c2, c3, c4, c5);
                        }
                        if (v == d4) {
                            v.setBackgroundColor(ship);
                            d2.setBackgroundColor(ship);
                            d3.setBackgroundColor(ship);
                            d5.setBackgroundColor(ship);
                            fun4(d2, d3, d4, d5);
                        }
                        if (v == e4) {
                            v.setBackgroundColor(ship);
                            e2.setBackgroundColor(ship);
                            e3.setBackgroundColor(ship);
                            e5.setBackgroundColor(ship);
                            fun4(e2, e3, e4, e5);
                        }
                        if (v == f4) {
                            v.setBackgroundColor(ship);
                            f2.setBackgroundColor(ship);
                            f3.setBackgroundColor(ship);
                            f5.setBackgroundColor(ship);
                            fun4(f2, f3, f4, f5);
                        }

                        if (v == g4) {
                            v.setBackgroundColor(ship);
                            g2.setBackgroundColor(ship);
                            g3.setBackgroundColor(ship);
                            g5.setBackgroundColor(ship);
                            fun4(g2, g3, g4, g5);
                        }
                        if (v == h4) {
                            v.setBackgroundColor(ship);
                            h2.setBackgroundColor(ship);
                            h3.setBackgroundColor(ship);
                            h5.setBackgroundColor(ship);
                            fun4(h2, h3, h4, h5);
                        }
                        if (v == i4) {
                            v.setBackgroundColor(ship);
                            i2.setBackgroundColor(ship);
                            i5.setBackgroundColor(ship);
                            i3.setBackgroundColor(ship);
                            fun4(i2, i3, i4, i5);
                        }
                        if (v == j4) {
                            v.setBackgroundColor(ship);
                            j2.setBackgroundColor(ship);
                            j5.setBackgroundColor(ship);
                            j3.setBackgroundColor(ship);
                            fun4(j2, j3, j4, j5);
                        }
                        if (v == a5) {
                            v.setBackgroundColor(ship);
                            a3.setBackgroundColor(ship);
                            a4.setBackgroundColor(ship);
                            a6.setBackgroundColor(ship);
                            fun4(a3, a4, a5, a6);
                        }
                        if (v == b5) {
                            v.setBackgroundColor(ship);
                            b3.setBackgroundColor(ship);
                            b4.setBackgroundColor(ship);
                            b6.setBackgroundColor(ship);
                            fun4(b3, b4, b5, b6);
                        }
                        if (v == c5) {
                            v.setBackgroundColor(ship);
                            c4.setBackgroundColor(ship);
                            c3.setBackgroundColor(ship);
                            c6.setBackgroundColor(ship);
                            fun4(c3, c4, c5, c6);
                        }
                        if (v == d5) {
                            v.setBackgroundColor(ship);
                            d4.setBackgroundColor(ship);
                            d3.setBackgroundColor(ship);
                            d6.setBackgroundColor(ship);
                            fun4(d3, d4, d5, d6);
                        }
                        if (v == e5) {
                            v.setBackgroundColor(ship);
                            e4.setBackgroundColor(ship);
                            e3.setBackgroundColor(ship);
                            e6.setBackgroundColor(ship);
                            fun4(e3, e4, e5, e6);
                        }
                        if (v == f5) {
                            v.setBackgroundColor(ship);
                            f4.setBackgroundColor(ship);
                            f3.setBackgroundColor(ship);
                            f6.setBackgroundColor(ship);
                            fun4(f3, f4, f5, f6);
                        }

                        if (v == g5) {
                            v.setBackgroundColor(ship);
                            g4.setBackgroundColor(ship);
                            g3.setBackgroundColor(ship);
                            g6.setBackgroundColor(ship);
                            fun4(g3, g4, g5, g6);
                        }
                        if (v == h5) {
                            v.setBackgroundColor(ship);
                            h4.setBackgroundColor(ship);
                            h3.setBackgroundColor(ship);
                            h6.setBackgroundColor(ship);
                            fun4(h3, h4, h5, h6);
                        }
                        if (v == i5) {
                            v.setBackgroundColor(ship);
                            i4.setBackgroundColor(ship);
                            i6.setBackgroundColor(ship);
                            i3.setBackgroundColor(ship);
                            fun4(i3, i4, i5, i6);
                        }
                        if (v == j5) {
                            v.setBackgroundColor(ship);
                            j4.setBackgroundColor(ship);
                            j6.setBackgroundColor(ship);
                            j3.setBackgroundColor(ship);
                            fun4(j3, j4, j5, j6);
                        }
                        if (v == a6) {
                            v.setBackgroundColor(ship);
                            a5.setBackgroundColor(ship);
                            a4.setBackgroundColor(ship);
                            a7.setBackgroundColor(ship);
                            fun4(a4, a5, a6, a7);
                        }
                        if (v == b6) {
                            v.setBackgroundColor(ship);
                            b7.setBackgroundColor(ship);
                            b5.setBackgroundColor(ship);
                            b4.setBackgroundColor(ship);
                            fun4(b4, b5, b6, b7);
                        }
                        if (v == c6) {
                            v.setBackgroundColor(ship);
                            c7.setBackgroundColor(ship);
                            c5.setBackgroundColor(ship);
                            c4.setBackgroundColor(ship);
                            fun4(c4, c5, c6, c7);
                        }
                        if (v == d6) {
                            v.setBackgroundColor(ship);
                            d7.setBackgroundColor(ship);
                            d5.setBackgroundColor(ship);
                            d4.setBackgroundColor(ship);
                            fun4(d4, d5, d6, d7);
                        }
                        if (v == e6) {
                            v.setBackgroundColor(ship);
                            e7.setBackgroundColor(ship);
                            e5.setBackgroundColor(ship);
                            e4.setBackgroundColor(ship);
                            fun4(e4, e5, e6, e7);
                        }
                        if (v == f6) {
                            v.setBackgroundColor(ship);
                            f7.setBackgroundColor(ship);
                            f5.setBackgroundColor(ship);
                            f4.setBackgroundColor(ship);
                            fun4(f4, f5, f6, f7);
                        }
                        if (v == g6) {
                            v.setBackgroundColor(ship);
                            g7.setBackgroundColor(ship);
                            g5.setBackgroundColor(ship);
                            g4.setBackgroundColor(ship);
                            fun4(g4, g5, g6, g7);
                        }
                        if (v == h6) {
                            v.setBackgroundColor(ship);
                            h7.setBackgroundColor(ship);
                            h5.setBackgroundColor(ship);
                            h4.setBackgroundColor(ship);
                            fun4(h4, h5, h6, h7);
                        }
                        if (v == i6) {
                            v.setBackgroundColor(ship);
                            i7.setBackgroundColor(ship);
                            i5.setBackgroundColor(ship);
                            i4.setBackgroundColor(ship);
                            fun4(i4, i5, i6, i7);
                        }
                        if (v == j6) {
                            v.setBackgroundColor(ship);
                            j7.setBackgroundColor(ship);
                            j4.setBackgroundColor(ship);
                            j5.setBackgroundColor(ship);
                            fun4(j4, j5, j6, j7);
                        }
                        if (v == a7) {
                            v.setBackgroundColor(ship);
                            a5.setBackgroundColor(ship);
                            a6.setBackgroundColor(ship);
                            a8.setBackgroundColor(ship);
                            fun4(a5, a6, a7, a8);
                        }
                        if (v == b7) {
                            v.setBackgroundColor(ship);
                            b8.setBackgroundColor(ship);
                            b5.setBackgroundColor(ship);
                            b6.setBackgroundColor(ship);
                            fun4(b5, b6, b7, b8);
                        }
                        if (v == c7) {
                            v.setBackgroundColor(ship);
                            c8.setBackgroundColor(ship);
                            c5.setBackgroundColor(ship);
                            c6.setBackgroundColor(ship);
                            fun4(c5, c6, c7, c8);
                        }
                        if (v == d7) {
                            v.setBackgroundColor(ship);
                            d8.setBackgroundColor(ship);
                            d5.setBackgroundColor(ship);
                            d6.setBackgroundColor(ship);
                            fun4(d5, d6, d7, d8);
                        }
                        if (v == e7) {
                            v.setBackgroundColor(ship);
                            e8.setBackgroundColor(ship);
                            e5.setBackgroundColor(ship);
                            e6.setBackgroundColor(ship);
                            fun4(e5, e6, e7, e8);
                        }
                        if (v == f7) {
                            v.setBackgroundColor(ship);
                            f8.setBackgroundColor(ship);
                            f5.setBackgroundColor(ship);
                            f6.setBackgroundColor(ship);
                            fun4(f5, f6, f7, f8);
                        }
                        if (v == g7) {
                            v.setBackgroundColor(ship);
                            g8.setBackgroundColor(ship);
                            g5.setBackgroundColor(ship);
                            g6.setBackgroundColor(ship);
                            fun4(g5, g6, g7, g8);
                        }
                        if (v == h7) {
                            v.setBackgroundColor(ship);
                            h8.setBackgroundColor(ship);
                            h5.setBackgroundColor(ship);
                            h6.setBackgroundColor(ship);
                            fun4(h5, h6, h7, h8);
                        }
                        if (v == i7) {
                            v.setBackgroundColor(ship);
                            i8.setBackgroundColor(ship);
                            i5.setBackgroundColor(ship);
                            i6.setBackgroundColor(ship);
                            fun4(i5, i6, i7, i8);
                        }
                        if (v == j7) {
                            v.setBackgroundColor(ship);
                            j8.setBackgroundColor(ship);
                            j6.setBackgroundColor(ship);
                            j5.setBackgroundColor(ship);
                            fun4(j5, j6, j7, j8);
                        }
                        if (v == a8) {
                            v.setBackgroundColor(ship);
                            a7.setBackgroundColor(ship);
                            a6.setBackgroundColor(ship);
                            a9.setBackgroundColor(ship);
                            fun4(a6, a7, a8, a9);
                        }
                        if (v == b8) {
                            v.setBackgroundColor(ship);
                            b9.setBackgroundColor(ship);
                            b7.setBackgroundColor(ship);
                            b6.setBackgroundColor(ship);
                            fun4(b6, b7, b8, b9);
                        }
                        if (v == c8) {
                            v.setBackgroundColor(ship);
                            c9.setBackgroundColor(ship);
                            c7.setBackgroundColor(ship);
                            c6.setBackgroundColor(ship);
                            fun4(c6, c7, c8, c9);
                        }
                        if (v == d8) {
                            v.setBackgroundColor(ship);
                            d9.setBackgroundColor(ship);
                            d7.setBackgroundColor(ship);
                            d6.setBackgroundColor(ship);
                            fun4(d6, d7, d8, d9);
                        }
                        if (v == e8) {
                            v.setBackgroundColor(ship);
                            e9.setBackgroundColor(ship);
                            e7.setBackgroundColor(ship);
                            e6.setBackgroundColor(ship);
                            fun4(e6, e7, e8, e9);
                        }
                        if (v == f8) {
                            v.setBackgroundColor(ship);
                            f9.setBackgroundColor(ship);
                            f7.setBackgroundColor(ship);
                            f6.setBackgroundColor(ship);
                            fun4(f6, f7, f8, f9);
                        }
                        if (v == g8) {
                            v.setBackgroundColor(ship);
                            g9.setBackgroundColor(ship);
                            g7.setBackgroundColor(ship);
                            g6.setBackgroundColor(ship);
                            fun4(g6, g7, g8, g9);
                        }
                        if (v == h8) {
                            v.setBackgroundColor(ship);
                            h9.setBackgroundColor(ship);
                            h7.setBackgroundColor(ship);
                            h6.setBackgroundColor(ship);
                            fun4(h6, h7, h8, h9);
                        }
                        if (v == i8) {
                            v.setBackgroundColor(ship);
                            i9.setBackgroundColor(ship);
                            i6.setBackgroundColor(ship);
                            i7.setBackgroundColor(ship);
                            fun4(i6, i7, i8, i9);
                        }
                        if (v == j8) {
                            v.setBackgroundColor(ship);
                            j9.setBackgroundColor(ship);
                            j6.setBackgroundColor(ship);
                            j7.setBackgroundColor(ship);
                            fun4(j6, j7, j8, j9);
                        }
                        if (v == a9) {
                            v.setBackgroundColor(ship);
                            a7.setBackgroundColor(ship);
                            a8.setBackgroundColor(ship);
                            a10.setBackgroundColor(ship);
                            fun4(a7, a8, a9, a10);

                        }
                        if (v == b9) {
                            v.setBackgroundColor(ship);
                            b10.setBackgroundColor(ship);
                            b7.setBackgroundColor(ship);
                            b8.setBackgroundColor(ship);
                            fun4(b7, b8, b9, b10);
                        }
                        if (v == c9) {
                            v.setBackgroundColor(ship);
                            c10.setBackgroundColor(ship);
                            c7.setBackgroundColor(ship);
                            c8.setBackgroundColor(ship);
                            fun4(c7, c8, c9, c10);
                        }
                        if (v == d9) {
                            v.setBackgroundColor(ship);
                            d10.setBackgroundColor(ship);
                            d7.setBackgroundColor(ship);
                            d8.setBackgroundColor(ship);
                            fun4(d7, d8, d9, d10);
                        }
                        if (v == e9) {
                            v.setBackgroundColor(ship);
                            e10.setBackgroundColor(ship);
                            e7.setBackgroundColor(ship);
                            e8.setBackgroundColor(ship);
                            fun4(e7, e8, e9, e10);
                        }
                        if (v == f9) {
                            v.setBackgroundColor(ship);
                            f10.setBackgroundColor(ship);
                            f7.setBackgroundColor(ship);
                            f8.setBackgroundColor(ship);
                            fun4(f7, f8, f9, f10);
                        }
                        if (v == g9) {
                            v.setBackgroundColor(ship);
                            g10.setBackgroundColor(ship);
                            g7.setBackgroundColor(ship);
                            g8.setBackgroundColor(ship);
                            fun4(g7, g8, g9, g10);
                        }
                        if (v == h9) {
                            v.setBackgroundColor(ship);
                            h10.setBackgroundColor(ship);
                            h7.setBackgroundColor(ship);
                            h8.setBackgroundColor(ship);
                            fun4(h7, h8, h9, h10);
                        }
                        if (v == i9) {
                            v.setBackgroundColor(ship);
                            i10.setBackgroundColor(ship);
                            i8.setBackgroundColor(ship);
                            i7.setBackgroundColor(ship);
                            fun4(i7, i8, i9, i10);
                        }
                        if (v == j9) {
                            v.setBackgroundColor(ship);
                            j10.setBackgroundColor(ship);
                            j8.setBackgroundColor(ship);
                            j7.setBackgroundColor(ship);
                            fun4(j7, j8, j9, j10);

                        }
                    }
                    setPlace();

                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    reset();


                    break;
                case DragEvent.ACTION_DROP:
                    if(     aa3==bb1||aa3==bb2||aa3==bb3||aa3==dd1||aa3==dd2||aa3==dd3||aa3==dd4||
                            aa1==bb1||aa1==bb2||aa1==bb3||aa1==dd1||aa1==dd2||aa1==dd3||aa1==dd4||
                            aa2==bb1||aa2==bb2||aa2==bb3||aa2==dd1||aa2==dd2||aa2==dd3||a2==dd4||
                            aa4==bb1||aa4==bb2||aa4==bb3||aa4==dd1||aa4==dd2||aa4==dd3||a4==dd4||
                            aa5==bb1||aa5==bb2||aa5==bb3||aa5==dd1||aa5==dd2||aa5==dd3||a5==dd4||
                            aa6==bb1||aa6==bb2||aa6==bb3||aa6==dd1||aa6==dd2||aa6==dd3||a6==dd4||
                            bb1==cc1||bb1==cc2||bb1==cc3||bb1==dd1||bb1==dd2||bb1==dd3||bb1==dd4||
                            bb2==cc1||bb2==cc2||bb2==cc3||bb2==dd1||bb2==dd2||bb2==dd3||bb2==dd4||
                            bb3==cc1||bb3==cc2||bb3==cc3||bb3==dd1||bb3==dd2||bb3==dd3||bb3==dd4||
                            dd1==cc1||dd1==cc2||dd1==cc3||dd2==cc1||dd2==cc2||dd2==cc3||dd3==cc1||
                            dd3==cc2||dd3==cc3||dd4==cc1||dd4==cc2||dd4==cc3
                    ) flag=false;
                    else{
                    // Dropped, reassign View to ViewGroup
                    View view = (View) event.getLocalState();
                    ViewGroup owner = (ViewGroup) view.getParent();
                    owner.removeView(view);
                    //LinearLayout container = (LinearLayout) v;
                    //container.addView(view);
                    view.setVisibility(View.VISIBLE);
                    //Log.w("siema", String.valueOf(view.getY()));
                    setPlace();}

                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    setPlace();


                default:
                    break;
            }

            return flag;
        }
    }
    @SuppressLint("ClickableViewAccessibility")
    private void initFindViewById(){
        ship1 = findViewById(R.id.ship1);
        ship2 = findViewById(R.id.ship2);
        ship3 = findViewById(R.id.ship3);
        ship4 = findViewById(R.id.ship4);

        ship1.setOnTouchListener(new MyTouchListener());
        ship2.setOnTouchListener(new MyTouchListener());
        ship3.setOnTouchListener(new MyTouchListener());
        ship4.setOnTouchListener(new MyTouchListener());

        aa1 = findViewById(R.id.u1);
        aa2 = findViewById(R.id.u2);
        aa3 = findViewById(R.id.u3);
        aa4 = findViewById(R.id.u4);
        aa5 = findViewById(R.id.u5);
        aa6 = findViewById(R.id.u6);

        bb1 = findViewById(R.id.u7);
        bb2 = findViewById(R.id.u8);
        bb3 = findViewById(R.id.u9);

        cc1 = findViewById(R.id.u10);
        cc2 = findViewById(R.id.u11);
        cc3 = findViewById(R.id.u12);

        dd1 = findViewById(R.id.u13);
        dd2 = findViewById(R.id.u14);
        dd3 = findViewById(R.id.u15);
        dd4 = findViewById(R.id.u16);

        a1 = findViewById(R.id.buttonA1);
        a2 = findViewById(R.id.buttonA2);
        a3 = findViewById(R.id.buttonA3);
        a4 = findViewById(R.id.buttonA4);
        a5 = findViewById(R.id.buttonA5);
        a6 = findViewById(R.id.buttonA6);
        a7 = findViewById(R.id.buttonA7);
        a8 = findViewById(R.id.buttonA8);
        a9 = findViewById(R.id.buttonA9);
        a10 = findViewById(R.id.buttonA10);

        b1 = findViewById(R.id.buttonB1);
        b2 = findViewById(R.id.buttonB2);
        b3 = findViewById(R.id.buttonB3);
        b4 = findViewById(R.id.buttonB4);
        b5 = findViewById(R.id.buttonB5);
        b6 = findViewById(R.id.buttonB6);
        b7 = findViewById(R.id.buttonB7);
        b8 = findViewById(R.id.buttonB8);
        b9 = findViewById(R.id.buttonB9);
        b10 = findViewById(R.id.buttonB10);

        c1 = findViewById(R.id.buttonC1);
        c2 = findViewById(R.id.buttonC2);
        c3 = findViewById(R.id.buttonC3);
        c4 = findViewById(R.id.buttonC4);
        c5 = findViewById(R.id.buttonC5);
        c6 = findViewById(R.id.buttonC6);
        c7 = findViewById(R.id.buttonC7);
        c8 = findViewById(R.id.buttonC8);
        c9 = findViewById(R.id.buttonC9);
        c10 = findViewById(R.id.buttonC10);

        d1 = findViewById(R.id.buttonD1);
        d2 = findViewById(R.id.buttonD2);
        d3 = findViewById(R.id.buttonD3);
        d4 = findViewById(R.id.buttonD4);
        d5 = findViewById(R.id.buttonD5);
        d6 = findViewById(R.id.buttonD6);
        d7 = findViewById(R.id.buttonD7);
        d8 = findViewById(R.id.buttonD8);
        d9 = findViewById(R.id.buttonD9);
        d10 = findViewById(R.id.buttonD10);

        e1 = findViewById(R.id.buttonE1);
        e2 = findViewById(R.id.buttonE2);
        e3 = findViewById(R.id.buttonE3);
        e4 = findViewById(R.id.buttonE4);
        e5 = findViewById(R.id.buttonE5);
        e6 = findViewById(R.id.buttonE6);
        e7 = findViewById(R.id.buttonE7);
        e8 = findViewById(R.id.buttonE8);
        e9 = findViewById(R.id.buttonE9);
        e10 = findViewById(R.id.buttonE10);

        f1 = findViewById(R.id.buttonF1);
        f2 = findViewById(R.id.buttonF2);
        f3 = findViewById(R.id.buttonF3);
        f4 = findViewById(R.id.buttonF4);
        f5 = findViewById(R.id.buttonF5);
        f6 = findViewById(R.id.buttonF6);
        f7 = findViewById(R.id.buttonF7);
        f8 = findViewById(R.id.buttonF8);
        f9 = findViewById(R.id.buttonF9);
        f10 = findViewById(R.id.buttonF10);

        g1 = findViewById(R.id.buttonG1);
        g2 = findViewById(R.id.buttonG2);
        g3 = findViewById(R.id.buttonG3);
        g4 = findViewById(R.id.buttonG4);
        g5 = findViewById(R.id.buttonG5);
        g6 = findViewById(R.id.buttonG6);
        g7 = findViewById(R.id.buttonG7);
        g8 = findViewById(R.id.buttonG8);
        g9 = findViewById(R.id.buttonG9);
        g10 = findViewById(R.id.buttonG10);

        h1 = findViewById(R.id.buttonH1);
        h2 = findViewById(R.id.buttonH2);
        h3 = findViewById(R.id.buttonH3);
        h4 = findViewById(R.id.buttonH4);
        h5 = findViewById(R.id.buttonH5);
        h6 = findViewById(R.id.buttonH6);
        h7 = findViewById(R.id.buttonH7);
        h8 = findViewById(R.id.buttonH8);
        h9 = findViewById(R.id.buttonH9);
        h10 = findViewById(R.id.buttonH10);

        i1 = findViewById(R.id.buttonI1);
        i2 = findViewById(R.id.buttonI2);
        i3 = findViewById(R.id.buttonI3);
        i4 = findViewById(R.id.buttonI4);
        i5 = findViewById(R.id.buttonI5);
        i6 = findViewById(R.id.buttonI6);
        i7 = findViewById(R.id.buttonI7);
        i8 = findViewById(R.id.buttonI8);
        i9 = findViewById(R.id.buttonI9);
        i10 = findViewById(R.id.buttonI10);

        j1 = findViewById(R.id.buttonJ1);
        j2 = findViewById(R.id.buttonJ2);
        j3 = findViewById(R.id.buttonJ3);
        j4 = findViewById(R.id.buttonJ4);
        j5 = findViewById(R.id.buttonJ5);
        j6 = findViewById(R.id.buttonJ6);
        j7 = findViewById(R.id.buttonJ7);
        j8 = findViewById(R.id.buttonJ8);
        j9 = findViewById(R.id.buttonJ9);
        j10 = findViewById(R.id.buttonJ10);

        a1.setOnDragListener(new MyDragListener());
        a2.setOnDragListener(new MyDragListener());
        a3.setOnDragListener(new MyDragListener());
        a4.setOnDragListener(new MyDragListener());
        a5.setOnDragListener(new MyDragListener());
        a6.setOnDragListener(new MyDragListener());
        a7.setOnDragListener(new MyDragListener());
        a8.setOnDragListener(new MyDragListener());
        a9.setOnDragListener(new MyDragListener());
        a10.setOnDragListener(new MyDragListener());

        b1.setOnDragListener(new MyDragListener());
        b2.setOnDragListener(new MyDragListener());
        b3.setOnDragListener(new MyDragListener());
        b4.setOnDragListener(new MyDragListener());
        b5.setOnDragListener(new MyDragListener());
        b6.setOnDragListener(new MyDragListener());
        b7.setOnDragListener(new MyDragListener());
        b8.setOnDragListener(new MyDragListener());
        b9.setOnDragListener(new MyDragListener());
        b10.setOnDragListener(new MyDragListener());

        c1.setOnDragListener(new MyDragListener());
        c2.setOnDragListener(new MyDragListener());
        c3.setOnDragListener(new MyDragListener());
        c4.setOnDragListener(new MyDragListener());
        c5.setOnDragListener(new MyDragListener());
        c6.setOnDragListener(new MyDragListener());
        c7.setOnDragListener(new MyDragListener());
        c8.setOnDragListener(new MyDragListener());
        c9.setOnDragListener(new MyDragListener());
        c10.setOnDragListener(new MyDragListener());

        d1.setOnDragListener(new MyDragListener());
        d2.setOnDragListener(new MyDragListener());
        d3.setOnDragListener(new MyDragListener());
        d4.setOnDragListener(new MyDragListener());
        d5.setOnDragListener(new MyDragListener());
        d6.setOnDragListener(new MyDragListener());
        d7.setOnDragListener(new MyDragListener());
        d8.setOnDragListener(new MyDragListener());
        d9.setOnDragListener(new MyDragListener());
        d10.setOnDragListener(new MyDragListener());

        e1.setOnDragListener(new MyDragListener());
        e2.setOnDragListener(new MyDragListener());
        e3.setOnDragListener(new MyDragListener());
        e4.setOnDragListener(new MyDragListener());
        e5.setOnDragListener(new MyDragListener());
        e6.setOnDragListener(new MyDragListener());
        e7.setOnDragListener(new MyDragListener());
        e8.setOnDragListener(new MyDragListener());
        e9.setOnDragListener(new MyDragListener());
        e10.setOnDragListener(new MyDragListener());

        f1.setOnDragListener(new MyDragListener());
        f2.setOnDragListener(new MyDragListener());
        f3.setOnDragListener(new MyDragListener());
        f4.setOnDragListener(new MyDragListener());
        f5.setOnDragListener(new MyDragListener());
        f6.setOnDragListener(new MyDragListener());
        f7.setOnDragListener(new MyDragListener());
        f8.setOnDragListener(new MyDragListener());
        f9.setOnDragListener(new MyDragListener());
        f10.setOnDragListener(new MyDragListener());

        g1.setOnDragListener(new MyDragListener());
        g2.setOnDragListener(new MyDragListener());
        g3.setOnDragListener(new MyDragListener());
        g4.setOnDragListener(new MyDragListener());
        g5.setOnDragListener(new MyDragListener());
        g6.setOnDragListener(new MyDragListener());
        g7.setOnDragListener(new MyDragListener());
        g8.setOnDragListener(new MyDragListener());
        g9.setOnDragListener(new MyDragListener());
        g10.setOnDragListener(new MyDragListener());

        h1.setOnDragListener(new MyDragListener());
        h2.setOnDragListener(new MyDragListener());
        h3.setOnDragListener(new MyDragListener());
        h4.setOnDragListener(new MyDragListener());
        h5.setOnDragListener(new MyDragListener());
        h6.setOnDragListener(new MyDragListener());
        h7.setOnDragListener(new MyDragListener());
        h8.setOnDragListener(new MyDragListener());
        h9.setOnDragListener(new MyDragListener());
        h10.setOnDragListener(new MyDragListener());

        i1.setOnDragListener(new MyDragListener());
        i2.setOnDragListener(new MyDragListener());
        i3.setOnDragListener(new MyDragListener());
        i4.setOnDragListener(new MyDragListener());
        i5.setOnDragListener(new MyDragListener());
        i6.setOnDragListener(new MyDragListener());
        i7.setOnDragListener(new MyDragListener());
        i8.setOnDragListener(new MyDragListener());
        i9.setOnDragListener(new MyDragListener());
        i10.setOnDragListener(new MyDragListener());

        j1.setOnDragListener(new MyDragListener());
        j2.setOnDragListener(new MyDragListener());
        j3.setOnDragListener(new MyDragListener());
        j4.setOnDragListener(new MyDragListener());
        j5.setOnDragListener(new MyDragListener());
        j6.setOnDragListener(new MyDragListener());
        j7.setOnDragListener(new MyDragListener());
        j8.setOnDragListener(new MyDragListener());
        j9.setOnDragListener(new MyDragListener());
        j10.setOnDragListener(new MyDragListener());
    }
}