package com.example.battleship;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void sendLogin(View view) {
        EditText editText = findViewById(R.id.userLogin);
        String userLogin = editText.getText().toString();

        Intent intent = new Intent(this, GameActivity2.class);
        intent.putExtra("LOGIN_KEY", userLogin);
        startActivity(intent);
    }
}